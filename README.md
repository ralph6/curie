# curie

Named after: [Marie Curie](https://en.wikipedia.org/wiki/Marie_Curie)

# Endpoints:

### POST HTTP://:11767/v1/buildUserData
```
{
    "env_file": {
        "file_path": "/etc/docker/app.env",
        "envs": {
            "ENV1": "SOMETHING"
        }
    },
    "docker_run_cmd": {
        "image": "app:1.00",
        "options": [
            {"--restart": "unless-stopped"},
            {"--name": "app"},
            {"-p": "8080:8080"},
            {"-p": "9080:9080"},
            {"-v": "/somewhere/logs:/app/logs"},
            {"--env-file": "/etc/docker/app.env"}
        ]
    }
}
```

### Response

```
{
    "user_data": "<base64 encoded data>"
}
```